import 'package:flutter/cupertino.dart';
import 'package:flutterwave/core/flutterwave.dart';
import 'package:flutterwave/flutterwave.dart';
import 'package:flutterwave/models/responses/charge_response.dart';
import 'package:flutterwave/utils/flutterwave_currency.dart';

class PaymentWidget extends StatefulWidget {
  @override
  _PaymentWidgetState createState() => _PaymentWidgetState();
}

class _PaymentWidgetState extends State<PaymentWidget> {
  final String txref = "My_unique_transaction_reference_123";
  final String amount = "400";
  final String currency = FlutterwaveCurrency.XAF;

  @override
  Widget build(BuildContext context) {
    return Container();
  }

  beginPayment() async {
    final Flutterwave flutterwave = Flutterwave.forUIPayment(
      context: this.context,
      encryptionKey: "FLWSECK-a150ca44daf403dd224336093a6dabb3-X",
      publicKey: "FLWPUBK-5d3e5d9df237600eb7bbf2220e49cb02-X",
      currency: this.currency,
      amount: this.amount,
      email: "chancelsim@outlook.fr",
      fullName: "Sandrine Kengmegni",
      txRef: this.txref,
      isDebugMode: true,
      phoneNumber: "+237698799803",
      acceptCardPayment: true,
      acceptUSSDPayment: true,
      acceptAccountPayment: true,
      acceptFrancophoneMobileMoney: true,
    );

    try {
      final ChargeResponse response =
          await flutterwave.initializeForUiPayments();
      if (response == null) {
        debugPrint("user didn't complete the transaction");
      } else {
        final isSuccessful = checkPaymentIsSuccessful(response);
        if (isSuccessful) {
          // provide value to customer
        } else {
          // check message
          debugPrint(response.message);

          // check status
          debugPrint(response.status);

          // check processor error
          debugPrint(response.data!.processorResponse);
        }
      }
    } catch (error, stacktrace) {
      debugPrint(error.toString());
    }
  }

  bool checkPaymentIsSuccessful(final ChargeResponse response) {
    return response.data!.status == FlutterwaveConstants.SUCCESSFUL &&
        response.data!.currency == this.currency &&
        response.data!.amount == this.amount &&
        response.data!.txRef == this.txref;
  }
}
