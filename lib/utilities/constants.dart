import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

//Colors
const primaryColor = Color(0XFFBC002E);
const whiteColor = Colors.white;
const secondaryColor = Color(0XFF071348);
const transparent = Colors.transparent;
const borderColor = Color.fromARGB(255, 43, 57, 117);

//TextStyle
final textStyleSecondary = GoogleFonts.oxygen(
  color: Colors.grey.shade400,
);
final textStylePrimary =
    GoogleFonts.oxygen(color: whiteColor, fontWeight: FontWeight.bold);
final textStylePrimaryShadow = GoogleFonts.oxygen(
  color: whiteColor,
);

final textStyleLink =
    GoogleFonts.oxygen(color: primaryColor, fontWeight: FontWeight.bold);
final boldTextStylePrimary = GoogleFonts.oxygen(
    color: whiteColor, fontWeight: FontWeight.bold, fontSize: 16);

//Padding

const appPadding = EdgeInsets.only(top: 20, bottom: 10, right: 15, left: 15);
const basicPadding = EdgeInsets.only(top: 5, left: 5);
const onlyTopPadding = EdgeInsets.only(top: 5);

const borderSideWhite = BorderSide(color: whiteColor, width: 1.0);

final DateTime now = DateTime.now();
final int currentDay = now.weekday;
final DateTime firstDayOfWeek = now.subtract(Duration(days: currentDay - 1));
final DateTime lastDayOfWeek = now.subtract(Duration(days: currentDay - 7));
