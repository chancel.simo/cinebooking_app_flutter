// ignore_for_file: unused_field

import 'package:cine_ticket_app/models/generic_response.dart';

import 'dart:convert';

List<MovieDto> movieFromJson(String str) =>
    List<MovieDto>.from(json.decode(str).map((x) => MovieDto.fromJson(x)));

String movieToJson(List<MovieDto> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class MovieDto {
  final int? id;
  final String title;
  final String detail;
  final String imgSrc;
  final int duration;
  final List<DateTime>? startDates;

  MovieDto({
    this.id,
    required this.title,
    required this.detail,
    required this.imgSrc,
    required this.duration,
    this.startDates,
  });

  factory MovieDto.fromJson(Map<String, dynamic> json) => MovieDto(
        id: json["id"],
        title: json["title"],
        detail: json["detail"],
        imgSrc: json["imgSrc"],
        duration: json["duration"],
        startDates: List<DateTime>.from(
            json["startDates"].map((x) => DateTime.parse(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "detail": detail,
        "imgSrc": imgSrc,
        "duration": duration,
        "startDates": startDates,
      };
}

class StartDate {
  final DateTime start;

  StartDate({
    required this.start,
  });

  factory StartDate.fromJson(Map<String, dynamic> json) => StartDate(
        start: DateTime.parse(json["start"]),
      );

  Map<String, dynamic> toJson() => {
        "start": start,
      };
}

class GetMovieResponse extends GenericResponse {
  MovieDto? movie;
}

class GetMoviesResponse extends GenericResponse {
  List<MovieDto>? movies;

  GetMoviesResponse() {
    movies = [];
  }
}

class GetMovieRequest {
  String? title;
  DateTime? startDate;
  DateTime? endDate;

  GetMovieRequest.fromDate({required this.startDate, required this.endDate});

  Map<String, String> toJson() => {
        "startDate": startDate!.toIso8601String(),
        "endDate": endDate!.toIso8601String(),
      };
}
