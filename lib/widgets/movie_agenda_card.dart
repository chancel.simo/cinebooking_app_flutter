// ignore_for_file: prefer_const_constructors

import 'package:cine_ticket_app/utilities/constants.dart';
import 'package:flutter/material.dart';

class MovieAgendaCard extends StatelessWidget {
  final String title;
  final String imgSrc;
  final int duration;
  const MovieAgendaCard(
      {required this.title,
      required this.imgSrc,
      required this.duration,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.15,
            width: MediaQuery.of(context).size.width * 0.3,
            decoration: BoxDecoration(
              border: Border.all(color: whiteColor, width: 1.0),
            ),
            child: Image.asset(
              imgSrc,
              fit: BoxFit.fill,
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.15,
            width: MediaQuery.of(context).size.width * 0.25,
            decoration: BoxDecoration(
                border: Border(
                    bottom: borderSideWhite,
                    top: borderSideWhite,
                    right: borderSideWhite)),
            child: Padding(
              padding: basicPadding,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(title, style: boldTextStylePrimary),
                  Padding(
                    padding: onlyTopPadding,
                    child: Text(
                      '${duration}minute',
                      style: textStylePrimary,
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
