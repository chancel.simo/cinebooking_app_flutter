// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:cine_ticket_app/utilities/constants.dart';
import 'package:cine_ticket_app/widgets/custom_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slimy_card/flutter_slimy_card.dart';
import 'package:qr_flutter/qr_flutter.dart';

class ReservationPage extends StatelessWidget {
  const ReservationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
        stops: [
          0.10,
          0.90,
        ],
        begin: Alignment.bottomLeft,
        end: Alignment.topRight,
        colors: [
          secondaryColor,
          Color(0xFF414f95),
        ],
      )),
      child: Scaffold(
        backgroundColor: transparent,
        appBar: AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          backgroundColor: transparent,
          title: Text(
            'Mes réservations',
            style: textStylePrimary,
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 10, left: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      'Réservation en cours',
                      style: textStyleSecondary,
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: StreamBuilder(
                      stream: flutterSlimyCardFlutter.stremStatus,
                      builder: (context, snapshot) {
                        return ListView(
                          shrinkWrap: true,
                          children: <Widget>[
                            FlutterSlimyCard(
                              borderRadius: 15,
                              topCardHeight: 200,
                              bottomCardHeight: 150,
                              color: primaryColor,
                              topCardWidget: topWidget(),
                              bottomCardWidget: bottomWidget(),
                              slimeEnabled: false,
                            )
                          ],
                        );
                      }),
                )
              ],
            ),
          ),
        ),
        bottomNavigationBar: CustomBottomBar(),
      ),
    );
  }

  topWidget() {
    return SafeArea(
      child: Column(
        children: [
          Container(
            height: 90,
            width: 90,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/test/im5.jpg'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            'Spiderman',
            style: TextStyle(
                color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 2,
          ),
          Text(
            DateTime.now().hour.toString() +
                'h' +
                DateTime.now().minute.toString(),
            style: TextStyle(color: whiteColor),
          ),
          SizedBox(
            height: 2,
          ),
          Text(
            '1 place',
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );
  }

  bottomWidget() {
    return Container(
      margin: EdgeInsets.only(top: 5),
      child: Column(
        children: [
          Flexible(
            child: QrImage(
              backgroundColor: whiteColor,
              data: "Chancel",
              version: QrVersions.auto,
              eyeStyle:
                  QrEyeStyle(color: primaryColor, eyeShape: QrEyeShape.square),
            ),
          )
        ],
      ),
    );
  }
}
