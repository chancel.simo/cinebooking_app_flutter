import 'package:cine_ticket_app/models/movie_dto.dart';
import 'package:cine_ticket_app/utilities/constants.dart';
import 'package:cine_ticket_app/widgets/custom_bottom_bar.dart';
import 'package:flutter/material.dart';

class DetailFilmPage extends StatefulWidget {
  final MovieDto data;

  const DetailFilmPage({required this.data, Key? key}) : super(key: key);

  @override
  _DetailFilmPageState createState() => _DetailFilmPageState();
}

class _DetailFilmPageState extends State<DetailFilmPage> {
  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: widget.data.imgSrc,
      child: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
          stops: [
            0.10,
            0.90,
          ],
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          colors: [
            secondaryColor,
            Color(0xFF414f95),
          ],
        )),
        child: Scaffold(
          backgroundColor: transparent,
          appBar: null,
          body: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Padding(
              padding: appPadding,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: IconButton(
                            onPressed: () => Navigator.pop(context),
                            icon: const Icon(
                              Icons.arrow_back,
                              color: whiteColor,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                          widget.data.title,
                          style: boldTextStylePrimary,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 1, left: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 2,
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.5,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              image: DecorationImage(
                                image: AssetImage(widget.data.imgSrc),
                                fit: BoxFit.fill,
                              ),
                              boxShadow: const [
                                BoxShadow(
                                    offset: Offset(0, 4),
                                    blurRadius: 4,
                                    color: Colors.black26)
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: SizedBox(
                              height: MediaQuery.of(context).size.height * 0.45,
                              child: Column(
                                children: const [
                                  Expanded(
                                    flex: 1,
                                    child: Padding(
                                      padding:
                                          EdgeInsets.only(top: 5, bottom: 5),
                                      child: RightBox(
                                          iconSrc: 'Videofull.png',
                                          subTitle: 'Genre',
                                          title: 'Comédie'),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Padding(
                                      padding:
                                          EdgeInsets.only(top: 5, bottom: 5),
                                      child: RightBox(
                                          iconSrc: 'Time.png',
                                          subTitle: 'Duration',
                                          title: '1h 20m'),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Padding(
                                      padding:
                                          EdgeInsets.only(top: 5, bottom: 5),
                                      child: RightBox(
                                          iconSrc: 'Star.png',
                                          subTitle: 'Note',
                                          title: '8,7/10'),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          widget.data.title,
                          style: boldTextStylePrimary,
                        ),
                        const Divider(
                          thickness: 1,
                          color: borderColor,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Text(
                            'Synopsis et détail',
                            style: textStylePrimaryShadow,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Text(
                            widget.data.detail,
                            style: textStylePrimary,
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          bottomNavigationBar: const CustomButtonBottomBar(),
        ),
      ),
    );
  }
}

class RightBox extends StatelessWidget {
  final String iconSrc;
  final String subTitle;
  final String title;

  const RightBox(
      {required this.iconSrc,
      required this.subTitle,
      required this.title,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: borderColor),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 2),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 2),
                child: ImageIcon(
                  AssetImage('assets/images/' + iconSrc),
                  color: whiteColor,
                ),
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  subTitle,
                  style: textStylePrimaryShadow,
                ),
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 2),
                child: Text(
                  title,
                  style: textStylePrimaryShadow,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
