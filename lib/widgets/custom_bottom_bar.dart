// ignore_for_file: prefer_const_constructors

import 'package:cine_ticket_app/utilities/constants.dart';
import 'package:flutter/material.dart';

class CustomBottomBar extends StatefulWidget {
  @override
  _CustomBottomBarState createState() => _CustomBottomBarState();
}

class _CustomBottomBarState extends State<CustomBottomBar> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10, bottom: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              IconButton(
                onPressed: () => Navigator.pushNamed(context, '/'),
                icon: Image.asset('assets/images/Home.png'),
              ),
              Text(
                'Accueil',
                style: textStylePrimary,
              )
            ]),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              IconButton(
                onPressed: () => Navigator.pushNamed(context, '/programmation'),
                icon: Image.asset('assets/images/Calendar.png'),
              ),
              Text(
                'Séance du jour',
                style: textStylePrimary,
              ),
            ]),
          ),
          Padding(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              IconButton(
                onPressed: () => Navigator.pushNamed(context, '/reservation'),
                icon: Image.asset('assets/images/Ticket.png'),
              ),
              Text(
                'Reservation',
                style: textStylePrimary,
              ),
            ]),
          ),
          Padding(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              IconButton(
                onPressed: null,
                icon: Image.asset('assets/images/Setting.png'),
              ),
              Text(
                'Réglages',
                style: textStylePrimary,
              ),
            ]),
          ),
        ],
      ),
    );
  }
}

class CustomButtonBottomBar extends StatelessWidget {
  const CustomButtonBottomBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10, bottom: 10, left: 50, right: 50),
      child: MaterialButton(
        color: primaryColor,
        onPressed: () => {},
        child: Text(
          'Réserver',
          style: TextStyle(color: whiteColor),
        ),
      ),
    );
  }
}
