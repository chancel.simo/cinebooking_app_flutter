// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:cine_ticket_app/models/movie_dto.dart';
import 'package:cine_ticket_app/services/movie_api.dart';
import 'package:cine_ticket_app/services/payment.dart';
import 'package:cine_ticket_app/utilities/constants.dart';
import 'package:cine_ticket_app/widgets/cat_btn.widget.dart';
import 'package:cine_ticket_app/widgets/custom_bottom_bar.dart';
import 'package:cine_ticket_app/widgets/slider_card.dart';
import 'package:flutter/material.dart';
import 'package:flutterwave/flutterwave.dart';
import 'package:flutterwave/models/responses/charge_response.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<MovieDto> movies = [];

  _loadMovies() async {
    debugPrint('load movies');
    await getMovies(GetMovieRequest.fromDate(
            startDate: DateTime.now(),
            endDate: DateTime.now().add(Duration(days: 3))))
        .then((value) {
      movies = value.movies!;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadMovies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
        stops: [
          0.10,
          0.90,
        ],
        begin: Alignment.bottomLeft,
        end: Alignment.topRight,
        colors: [
          secondaryColor,
          Color(0xFF414f95),
        ],
      )),
      child: Scaffold(
        backgroundColor: transparent,
        appBar: null,
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: DefaultTextStyle(
            style: textStylePrimary,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 40, 10, 10),
              child: Column(
                children: [
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Canal olympia Yaoundé 👋',
                              style: textStyleSecondary,
                            ),
                            const Padding(
                              padding: EdgeInsets.only(top: 10),
                              child: Text('Reservez une séance.'),
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: InkWell(
                            borderRadius: BorderRadius.circular(17),
                            onTap: () {
                              debugPrint('hello world');
                              //  beginPayment();
                            },
                            child: Container(
                              padding: const EdgeInsets.all(15),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(17),
                                color: whiteColor.withOpacity(0.08),
                              ),
                              child: Image.asset('assets/images/Profile.png'),
                            ),
                          ),
                        )
                      ]),
                  Padding(
                    padding: appPadding,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          child: TextField(
                            cursorColor: whiteColor,
                            style: textStylePrimary,
                            decoration: InputDecoration(
                              floatingLabelBehavior:
                                  FloatingLabelBehavior.never,
                              labelText: 'Rechercher un film',
                              hintText: 'Rechercher un film',
                              labelStyle: textStylePrimary,
                              hintStyle: textStylePrimary,
                              focusColor: whiteColor,
                              prefixIcon: Image.asset(
                                'assets/images/Search.png',
                              ),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(40),
                                  borderSide: BorderSide.none),
                              filled: true,
                              fillColor: whiteColor.withOpacity(0.08),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: appPadding,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Catégories',
                              style: boldTextStylePrimary,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: Directionality(
                                textDirection: TextDirection.rtl,
                                child: TextButton.icon(
                                  onPressed: null,
                                  label: Text(
                                    'Voir toutes',
                                    style: textStyleSecondary,
                                  ),
                                  icon: Icon(Icons.arrow_back_rounded),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            TextButton(
                              onPressed: null,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  EmojiButtonStyle(emojiStr: '😍'),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Text(
                                      'Romance',
                                      style: textStyleSecondary,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            TextButton(
                              onPressed: null,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  EmojiButtonStyle(emojiStr: '😁'),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Text(
                                      'Comédie',
                                      style: textStyleSecondary,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            TextButton(
                              onPressed: null,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  EmojiButtonStyle(emojiStr: '😱'),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Text(
                                      'Horreur',
                                      style: textStyleSecondary,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            TextButton(
                              onPressed: null,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  EmojiButtonStyle(emojiStr: '😙'),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Text(
                                      'Drama',
                                      style: textStyleSecondary,
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 15),
                              child: Text(
                                'A l\'affiche ce mois',
                                style: boldTextStylePrimary,
                              ),
                            ),
                            SizedBox(
                              height: 270,
                              width: 400,
                              child: CarousselView(movies: movies),
                            ),
                          ],
                        ),
                        /* Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 15),
                              child: Text(
                                'Prochainement',
                                style: boldTextStylePrimary,
                              ),
                            ),
                            Container(
                              height: 270,
                              width: 400,
                              child: CarousselView(),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 15),
                              child: Text(
                                'Ma liste',
                                style: boldTextStylePrimary,
                              ),
                            ),
                            Container(
                              height: 270,
                              width: 400,
                              child: CarousselView(),
                            ),
                          ],
                        ), */
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: CustomBottomBar(),
      ),
    );
  }
}
