// ignore_for_file: prefer_const_constructors, unused_element

import 'package:cine_ticket_app/models/movie_dto.dart';
import 'package:cine_ticket_app/services/movie_api.dart';
import 'package:cine_ticket_app/utilities/constants.dart';
import 'package:cine_ticket_app/widgets/custom_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:logger/logger.dart';

class ProgrammationSession extends StatefulWidget {
  const ProgrammationSession({Key? key}) : super(key: key);

  @override
  _ProgrammationSessionState createState() => _ProgrammationSessionState();
}

class _ProgrammationSessionState extends State<ProgrammationSession> {
  final CalendarController _calendarController = CalendarController();
  late List<MovieDto> _movies = [];
  var logger = Logger();

  @override
  void initState() {
    super.initState();

    _getMovies();
  }

  dynamic _getMovies() async {
    await Future.wait([
      Future.microtask(
        () =>
            getMovies(GetMovieRequest.fromDate(startDate: null, endDate: null))
                .then((value) => _movies = value.movies!),
      ),
    ]);
  }

  List<MovieWrapper> _getDataSource() {
    _getMovies();

    final List<MovieWrapper> movies = [];
    for (MovieDto movie in _movies) {
      movies.add(
        MovieWrapper.fromMovie(movie, _calendarController.displayDate!),
      );
    }
    return movies;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
        stops: [
          0.10,
          0.90,
        ],
        begin: Alignment.bottomLeft,
        end: Alignment.topRight,
        colors: [
          secondaryColor,
          Color(0xFF414f95),
        ],
      )),
      child: Scaffold(
        backgroundColor: transparent,
        appBar: null,
        body: Padding(
          padding: const EdgeInsets.only(top: 40),
          child: SfCalendar(
            appointmentBuilder: (context, calendarAppointmentDetails) {
              final MovieWrapper movie =
                  calendarAppointmentDetails.appointments.first;
              return Column(
                children: [
                  Column(
                    children: [
                      Container(
                        width: calendarAppointmentDetails.bounds.width,
                        height: calendarAppointmentDetails.bounds.height,
                        color: primaryColor,
                        child: Center(
                          child: Text(
                            movie.title,
                            style: TextStyle(color: whiteColor),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              );
            },
            view: CalendarView.day,
            controller: _calendarController,
            onViewChanged: (ViewChangedDetails details) {
              //_loadMovies(_calendarController.displayDate!);
            },
            dataSource: MovieDataSource(_getDataSource()),
            headerStyle: CalendarHeaderStyle(
              textStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            todayHighlightColor: primaryColor,
            firstDayOfWeek: 1,
            showNavigationArrow: true,
            todayTextStyle: TextStyle(
              color: whiteColor,
            ),
            showCurrentTimeIndicator: true,
            timeSlotViewSettings: TimeSlotViewSettings(
              timeTextStyle: TextStyle(color: whiteColor, fontSize: 12),
              timeIntervalHeight: 50,
              startHour: 7,
              endHour: 24,
              nonWorkingDays: const <int>[DateTime.monday],
              timeFormat: 'hh:mm',
              timeRulerSize: 60,
            ),
          ),
        ),
        bottomNavigationBar: CustomBottomBar(),
      ),
    );
  }
}

class MovieDataSource extends CalendarDataSource {
  MovieDataSource(List<MovieWrapper> source) {
    appointments = source;
  }

  @override
  DateTime getStartTime(int index) {
    return _getMovieData(index).startDate;
  }

  @override
  DateTime getEndTime(int index) {
    return _getMovieData(index).endDate;
  }

  @override
  String getSubject(int index) {
    return _getMovieData(index).title;
  }

  @override
  Color getColor(int index) {
    return primaryColor;
  }

  @override
  bool isAllDay(int index) {
    return false;
  }

  MovieWrapper _getMovieData(int index) {
    final dynamic movie = appointments![index];
    late final MovieWrapper movieData;
    if (movie is MovieWrapper) {
      movieData = movie;
    }

    return movieData;
  }
}

class MovieWrapper {
  final DateTime currentDate;
  final List<DateTime> _startDates;
  final int _duration;
  final String title;

  late DateTime startDate;
  late DateTime endDate;
  MovieWrapper.fromMovie(MovieDto movie, this.currentDate)
      : _duration = movie.duration,
        _startDates = movie.startDates!,
        title = movie.title {
    _initialDateMovie();
  }

  void _initialDateMovie() {
    DateTime _value = _startDates
        .firstWhere((element) => element.isAtSameMomentAs(currentDate));

    startDate = _value;
    endDate = _value.add(Duration(minutes: _duration));
  }
}
