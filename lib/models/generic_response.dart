import 'package:flutter/cupertino.dart';

class GenericResponse {
  String? message;
  bool? success;

  GenericResponse();

  handleResponse(String e) {
    message = e;

    debugPrint(message);
  }
}
