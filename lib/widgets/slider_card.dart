// ignore_for_file: non_constant_identifier_names

import 'package:cine_ticket_app/models/movie_dto.dart';
import 'package:flutter/material.dart';

class CarousselView extends StatefulWidget {
  final List<MovieDto> movies;

  const CarousselView({required this.movies, Key? key}) : super(key: key);

  @override
  _CarousselViewState createState() => _CarousselViewState();
}

class _CarousselViewState extends State<CarousselView> {
  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 0.85,
      child: PageView.builder(
        itemCount: widget.movies.length,
        itemBuilder: (context, index) {
          return CardItem(
              index: index,
              movies_img: widget.movies.map((e) => e.imgSrc).toList());
        },
        physics: const BouncingScrollPhysics(),
        controller: PageController(
            viewportFraction: 0.5, initialPage: widget.movies.length),
      ),
    );
  }
}

class CardItem extends StatefulWidget {
  final int index;
  final List<String> movies_img;

  const CardItem({required this.index, required this.movies_img, Key? key})
      : super(key: key);

  @override
  State<CardItem> createState() => _CardItemState();
}

class _CardItemState extends State<CardItem> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: GestureDetector(
        onTap: () => Navigator.pushNamed(context, '/detail',
            arguments: MovieDto(
                title: 'Test',
                imgSrc: widget.movies_img[widget.index],
                duration: 120,
                detail:
                    "Au cours d’une luxueuse croisière sur le Nil, ce qui devait être une lune de miel idyllique se conclut par la mort brutale de la jeune mariée. Ce crime sonne la fin des vacances pour le détective Hercule Poirot. A bord en tant que passager, il se voit confier l’enquête par le capitaine du bateau. Et dans cette sombre affaire d’amour obsessionnel aux conséquences meurtrières, ce ne sont pas les suspects qui manquent ! S’ensuivent une série de rebondissements et de retournements de situation qui, sur fond de paysages grandioses, vont peu à peu déstabiliser les certitudes de chacun jusqu’à l’incroyable dénouement !")),
        child: Hero(
          tag: widget.movies_img[widget.index],
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                image: DecorationImage(
                  image: AssetImage(widget.movies_img[widget.index]),
                  fit: BoxFit.fill,
                ),
                boxShadow: const [
                  BoxShadow(
                      offset: Offset(0, 4),
                      blurRadius: 4,
                      color: Colors.black26)
                ]),
          ),
        ),
      ),
    );
  }
}
