import 'package:cine_ticket_app/models/movie_dto.dart';
import 'package:cine_ticket_app/pages/detail_film_page.dart';
import 'package:cine_ticket_app/pages/home_page.dart';
import 'package:cine_ticket_app/pages/programation_page.dart';
import 'package:cine_ticket_app/pages/reservation_page.dart';
import 'package:flutter/material.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => const HomePage());
      case '/detail':
        if (args is MovieDto) {
          return MaterialPageRoute(
              builder: (_) => DetailFilmPage(
                    data: args,
                  ));
        }
        return _errorRoute();

      case '/programmation':
        return MaterialPageRoute(builder: (_) => const ProgrammationSession());
      case '/reservation':
        return MaterialPageRoute(builder: (_) => const ReservationPage());

      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return const Scaffold(
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
