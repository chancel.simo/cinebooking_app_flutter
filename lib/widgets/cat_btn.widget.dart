// ignore_for_file: use_key_in_widget_constructors

import 'package:cine_ticket_app/utilities/constants.dart';
import 'package:flutter/cupertino.dart';

class EmojiButtonStyle extends StatelessWidget {
  final String emojiStr;

  const EmojiButtonStyle({required this.emojiStr}) : super();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(17),
        color: whiteColor.withOpacity(0.08),
      ),
      child: Text(
        emojiStr,
        style: const TextStyle(fontSize: 22),
      ),
    );
  }
}
