// ignore_for_file: control_flow_in_finally

import 'dart:io';

import 'package:cine_ticket_app/models/movie_dto.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

Future<GetMoviesResponse> getMovies([GetMovieRequest? request]) async {
  var getMoviesResponse = GetMoviesResponse();
  try {
    final url = Uri.http('10.0.2.2:3000', '/movies');

    Map<String, String>? formattedRequest;

    if (request != null &&
        request.startDate != null &&
        request.endDate != null) {
      formattedRequest = request.toJson();
    }

    var response = await http.post(url,
        headers: {"content-type": "application/json"},
        body: formattedRequest != null ? jsonEncode(formattedRequest) : null);
    if (response.statusCode == 200) {
      debugPrint('Number of books about http: ${response.body} .');
      getMoviesResponse.movies = movieFromJson(response.body);
    } else {
      debugPrint('Request failed with status: ${response.statusCode}.');
    }

    getMoviesResponse.success = true;
  } catch (e) {
    getMoviesResponse.handleResponse(e.toString());
  } finally {
    return getMoviesResponse;
  }
}
